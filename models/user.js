var mongoose = require('mongoose');
var Schema = new mongoose.Schema({
	username:{
		type:String,
		required:true,
		unique:true,
		default:''
	},
	firstname:{
		type:String,
		default:''
	},
	lastname:{
		type:String,
		default:''
	},
	email: {
		type: String, 
		lowercase: true,
		unique:true,
	 	required: [true, "can't be blank"], 
	 	match: [/\S+@\S+\.\S+/, 'is invalid'],
	 	index: true
	},
  	password:{
		type:String,
		required:true,
		default:''
	}
});

var user= new mongoose.model('User', Schema)
module.exports= user;