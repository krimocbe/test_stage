var mongoose = require('mongoose');
var Schema = new mongoose.Schema({
	ref_produit:{
		type:Number,
		required:true,
		unique:true,
		default:''
	},
	libelle_produit:{
		type:String,
		default:''
	},
	date_production:{
		type:Date,
		default:''
	},
	prix:{
		type:Number,
		required:true,
		default:''
	}
});
var product= new mongoose.model('Product', Schema)
module.exports= product;