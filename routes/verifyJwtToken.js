const jwt = require('jsonwebtoken');

const secret='krimou-super-secret-key';


verifyToken = (req, res, next) => {
  let token = req.cookies.auth;
  console.log("hello world"+token);
  if (!token){
    return res.status(403).send({ 
      auth: false, message: 'No token provided.' 
    });
  }
 
  jwt.verify(token, secret, (err, decoded) => {
    if (err){
      console.log(err);	
      return res.status(500).send({ 
          auth: false, 
          message: 'Fail to Authentication. Error -> ' + err 
        });
    }
    req.userId = decoded.id;
    next();
  });
}
 
const authJwt = {};
authJwt.verifyToken = verifyToken;
module.exports = authJwt;