const authJwt = require('./verifyJwtToken');
var cors = require('cors');
var moment = require('moment');
module.exports =function(app){
  const controller = require('../controllers/controller.js');
  const repository = require('../repositories/ProductRepository');
  const product_controller = require('../controllers/ProductController.js');
  //user
  app.post('/api/auth/signup',[cors()],controller.signup);
	app.post('/api/auth/signin',[cors()],controller.signin);
  app.get('/api/auth/getContentUser',[authJwt.verifyToken,cors()],controller.userContent);
  app.get('/logout', controller.logout);
  //produit
  app.get('/',product_controller.render_home);
  app.get('/signup',product_controller.render_signup);
  app.get('/login', product_controller.render_login);
  app.get('/modify_product/:id',product_controller.modify_product)
  app.get('/delete_product/:id',[authJwt.verifyToken,cors()],product_controller.delete_product); 
  app.post('/product',[authJwt.verifyToken,cors()], product_controller.create_product);
  app.get('/list_product',[authJwt.verifyToken,cors()],product_controller.afficher_product);
  app.post('/lproduct/(:id)',[authJwt.verifyToken,cors()],product_controller.modifier_product_action)
  app.get('/home', product_controller.rendre_session);
}	