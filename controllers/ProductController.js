var moment = require('moment');
var User=require('../models/user');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');
const repository = require('../repositories/ProductRepository');

exports.render_home=(req,res,next)=>{
	res.render('index', {page:'Home', menuId:'home', title: 'Test'});
}

exports.render_signup=(req,res,next)=>{
    res.render('register', {message:'',page:'Home', menuId:'home', title: 'Test'});
}	

exports.render_login=(req, res, next)=>{
    res.render('login',{message:'', page:'Home', menuId:'home', title: 'Test'});
}

exports.modify_product=(req, res, next)=>{
	repository.findById(req.params.id).then(product=>{
        res.render('modify_product', {message:'',page:'Home', menuId:'home', title: 'Test' , moment: moment, product:product});
    });
}
exports.delete_product=(req, res, next)=>{
	const { id } = req.params;
    console.log(id)
    repository.deleteById(id).then((ok) => {
       res.redirect('/list_product')
    }).catch((error) => console.log(error));
}

exports.create_product=(req, res, next)=>{	
	repository.create(req.body.name , req.body.ref , req.body.date , req.body.prix).then((product) => {
    	res.redirect('/list_product');
    }).catch((error) => console.log(error));
}	

exports.afficher_product=(req, res, next)=>{
	repository.findAll().then((product) => {
        console.log(product);
        res.render('show_products', {page:'Home', menuId:'home', title: 'show products', moment: moment ,products:product});
    }).catch((error) => console.log(error));
}
exports.modifier_product_action=(req, res, next)=>{
	const { id } = req.params;
    console.log(req.body.ref)
    const product = { 
    	"name":req.body.name , 
        "ref":req.body.ref ,
        "date":req.body.date ,
        "prix":req.body.prix 
    };
    console.log(product);
    repository.updateById(id, product).then(res.redirect('/list_product'))
    .catch((error) => console.log(error));
}	

exports.rendre_session=(req, res, next)=>{
	res.render('home',{message:'', page:'Home', menuId:'home', title: 'Test'});
}	

