var User=require('../models/user');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');

mongoose.connect(
  "mongodb+srv://krimou:krimouohb123@cluster0-5vusm.mongodb.net/test?retryWrites=true",
{
  useCreateIndex: true,
  useNewUrlParser: true
});
var db= mongoose.connection;
const secret='krimou-super-secret-key';

exports.signin = (req,res)=>{
	
	User.findOne({
			'email':req.body.email
		}).then(user=>{
			
			if(!user){
				return res.status(400).send(" Username doesnt exist");
			}
			console.log(user.password)
			console.log(bcrypt.hashSync(req.body.password,8))
			var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
			if(!passwordIsValid){
				return res.status(400).send(" Password doesnt match with the written password");
			}
			var token = jwt.sign({ id: user.id }, secret, {
      							expiresIn: 86400 // dans 24 heures 
    		});
    		res.cookie('auth',token);
    		res.redirect('/list_product');
 			//res.status(200).render('home',{page:'Home',message:'',menuId:'home', title: 'Home', auth: true, accessToken: token });
			    	

		}).catch(err => {
    		res.status(500).send('Error -> ' + err);
  	});
}

exports.signup=(req,res,next)=>{
	var user = new User({
	username:req.body.username,
	firstname:req.body.firstname,
	lastname:req.body.lastname,
	email:req.body.email,
	password:bcrypt.hashSync(req.body.password,8)
	});
	user.save(function(err,User){
	if(err) throw err;
		res.redirect('/');
 			
	});  	
};


exports.logout=(req,res,next)=>{
	res.clearCookie("auth");
    res.render('index', {page:'Home', menuId:'home', title: 'Test'});
}

exports.userContent =(req,res,next)=>{
	console.log("gfkgjhjlfj")
	User.findOne({
		'username':req.body.username
	}).then(user=>{
		res.status(200).json({
			"description":"User content page",
			"user":user
		})
	}).catch(err =>{
		res.status(500).json({
			"description":"can not access User Page",
			"error":err
		});
	})
}
