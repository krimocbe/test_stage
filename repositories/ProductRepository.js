
const Product = require('../models/product');

class ProductRepository {

  constructor(model) {
    this.model = model;
  }

  // create a new todo
  create(name , ref ,  date , prix) {
    const newProduct = { 
      "ref_produit":ref,
      "libelle_produit":name,
      "date_production":date,
      "prix":prix
    };
    const product = new this.model(newProduct);

    return product.save();
  }

  // return all todos

  findAll() {
    return this.model.find();
  }

  //find todo by the id
  findById(id) {
    return this.model.findById(id);
  }

    // delete todo
  deleteById(id) {
    return this.model.findByIdAndDelete(id);
  }

  //update todo
  updateById(id, object) {
    const query = { _id: id };
    console.log(object);
    return this.model.findOneAndUpdate(query, { $set: 
      { 
        ref_produit:object.ref,
        libelle_produit:object.name,
        date_production:object.date,
        prix:object.prix 
      } 
    });
  }

}

module.exports = new ProductRepository(Product);
